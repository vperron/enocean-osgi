package gnu.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TooManyListenersException;

public final class RXTXPort extends SerialPort
{
  protected static final boolean debug = false;
  protected static final boolean debug_read = false;
  protected static final boolean debug_read_results = false;
  protected static final boolean debug_write = false;
  protected static final boolean debug_events = false;
  protected static final boolean debug_verbose = false;
  private static Zystem z;
  boolean MonitorThreadAlive = false;

  int IOLocked = 0;

  private int fd = 0;

  long eis = 0L;

  int pid = 0;

  static boolean dsrFlag = false;

  private final SerialOutputStream out = new SerialOutputStream();

  private final SerialInputStream in = new SerialInputStream();

  private int speed = 9600;

  private int dataBits = 8;

  private int stopBits = 1;

  private int parity = 0;

  private int flowmode = 0;
  private int timeout;
  private int threshold = 0;

  private int InputBuffer = 0;
  private int OutputBuffer = 0;
  private SerialPortEventListener SPEventListener;
  private MonitorThread monThread;
  boolean monThreadisInterrupted = true;

  boolean MonitorThreadLock = true;

  boolean closeLock = false;

  private static native void Initialize();

  public RXTXPort(String paramString)
    throws PortInUseException
  {
    this.fd = open(paramString);
    this.name = paramString;

    this.MonitorThreadLock = true;
    this.monThread = new MonitorThread();
    this.monThread.start();
    waitForTheNativeCodeSilly();
    this.MonitorThreadAlive = true;

    this.timeout = -1;
  }

  private synchronized native int open(String paramString)
    throws PortInUseException;

  public OutputStream getOutputStream()
  {
    return this.out;
  }

  public InputStream getInputStream()
  {
    return this.in;
  }

  private native int nativeGetParity(int paramInt);

  private native int nativeGetFlowControlMode(int paramInt);

  public synchronized void setSerialPortParams(int speed, int dataBits, int stopBits, int parity)
    throws UnsupportedCommOperationException
  {
    if (nativeSetSerialPortParams(speed, dataBits, stopBits, parity)) {
      throw new UnsupportedCommOperationException("Invalid Parameter");
    }
    this.speed = speed;
    if (dataBits == 3) this.dataBits = 5; else
      this.dataBits = dataBits;
    this.stopBits = stopBits;
    this.parity = parity;
    z.reportln("RXTXPort:setSerialPortParams(" + speed + " " + dataBits + " " + stopBits + " " + parity + ") returning");
  }

  private native boolean nativeSetSerialPortParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws UnsupportedCommOperationException;

  public int getBaudRate()
  {
    return this.speed;
  }

  public int getDataBits()
  {
    return this.dataBits;
  }

  public int getStopBits()
  {
    return this.stopBits;
  }

  public int getParity()
  {
    return this.parity;
  }

  public void setFlowControlMode(int paramInt)
  {
    if (this.monThreadisInterrupted)
    {
      return;
    }
    try {
      setflowcontrol(paramInt);
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
      return;
    }
    this.flowmode = paramInt;
  }

  public int getFlowControlMode()
  {
    return this.flowmode;
  }

  native void setflowcontrol(int paramInt)
    throws IOException;

  public void enableReceiveFraming(int paramInt)
    throws UnsupportedCommOperationException
  {
    throw new UnsupportedCommOperationException("Not supported");
  }

  public void disableReceiveFraming()
  {
  }

  public boolean isReceiveFramingEnabled()
  {
    return false;
  }

  public int getReceiveFramingByte()
  {
    return 0;
  }

  public native int NativegetReceiveTimeout();

  private native boolean NativeisReceiveTimeoutEnabled();

  private native void NativeEnableReceiveTimeoutThreshold(int paramInt1, int paramInt2, int paramInt3);

  public void disableReceiveTimeout()
  {
    this.timeout = -1;
    NativeEnableReceiveTimeoutThreshold(this.timeout, this.threshold, this.InputBuffer);
  }

  public void enableReceiveTimeout(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.timeout = paramInt;
      NativeEnableReceiveTimeoutThreshold(paramInt, this.threshold, this.InputBuffer);
    }
    else
    {
      throw new IllegalArgumentException("Unexpected negative timeout value");
    }
  }

  public boolean isReceiveTimeoutEnabled()
  {
    return NativeisReceiveTimeoutEnabled();
  }

  public int getReceiveTimeout()
  {
    return NativegetReceiveTimeout();
  }

  public void enableReceiveThreshold(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.threshold = paramInt;
      NativeEnableReceiveTimeoutThreshold(this.timeout, this.threshold, this.InputBuffer);
    }
    else
    {
      throw new IllegalArgumentException("Unexpected negative threshold value");
    }
  }

  public void disableReceiveThreshold()
  {
    enableReceiveThreshold(0);
  }

  public int getReceiveThreshold()
  {
    return this.threshold;
  }

  public boolean isReceiveThresholdEnabled()
  {
    return this.threshold > 0;
  }

  public void setInputBufferSize(int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException("Unexpected negative buffer size value");
    }

    this.InputBuffer = paramInt;
  }

  public int getInputBufferSize()
  {
    return this.InputBuffer;
  }

  public void setOutputBufferSize(int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException("Unexpected negative buffer size value");
    }

    this.OutputBuffer = paramInt;
  }

  public int getOutputBufferSize()
  {
    return this.OutputBuffer;
  }

  public native boolean isDTR();

  public native void setDTR(boolean paramBoolean);

  public native void setRTS(boolean paramBoolean);

  private native void setDSR(boolean paramBoolean);

  public native boolean isCTS();

  public native boolean isDSR();

  public native boolean isCD();

  public native boolean isRI();

  public native boolean isRTS();

  public native void sendBreak(int paramInt);

  protected native void writeByte(int paramInt, boolean paramBoolean)
    throws IOException;

  protected native void writeArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
    throws IOException;

  protected native boolean nativeDrain(boolean paramBoolean)
    throws IOException;

  protected native int nativeavailable()
    throws IOException;

  protected native int readByte()
    throws IOException;

  protected native int readArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException;

  protected native int readTerminatedArray(byte[] paramArrayOfByte1, int paramInt1, int paramInt2, byte[] paramArrayOfByte2)
    throws IOException;

  native void eventLoop();

  private native void interruptEventLoop();

  public boolean checkMonitorThread()
  {
    if (this.monThread != null)
    {
      return this.monThreadisInterrupted;
    }

    return true;
  }

  public boolean sendEvent(int paramInt, boolean paramBoolean)
  {
    if ((this.fd == 0) || (this.SPEventListener == null) || (this.monThread == null))
    {
      return true;
    }

    switch (paramInt)
    {
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
    case 4:
      break;
    case 5:
      break;
    case 6:
      break;
    case 7:
      break;
    case 8:
      break;
    case 9:
      break;
    case 10:
      break;
    }

    switch (paramInt)
    {
    case 1:
      if (!this.monThread.Data)
        return false; break;
    case 2:
      if (!this.monThread.Output)
        return false; break;
    case 3:
      if (!this.monThread.CTS)
        return false; break;
    case 4:
      if (!this.monThread.DSR)
        return false; break;
    case 5:
      if (!this.monThread.RI)
        return false; break;
    case 6:
      if (!this.monThread.CD)
        return false; break;
    case 7:
      if (!this.monThread.OE)
        return false; break;
    case 8:
      if (!this.monThread.PE)
        return false; break;
    case 9:
      if (!this.monThread.FE)
        return false; break;
    case 10:
      if (!this.monThread.BI)
        return false; break;
    default:
      System.err.println("unknown event: " + paramInt);
      return false;
    }

    SerialPortEvent localSerialPortEvent = new SerialPortEvent(this, paramInt, !paramBoolean, paramBoolean);

    if (this.monThreadisInterrupted)
    {
      return true;
    }
    if (this.SPEventListener != null)
    {
      this.SPEventListener.serialEvent(localSerialPortEvent);
    }

    if ((this.fd == 0) || (this.SPEventListener == null) || (this.monThread == null))
    {
      return true;
    }

    return false;
  }

  public void addEventListener(SerialPortEventListener paramSerialPortEventListener)
    throws TooManyListenersException
  {
    if (this.SPEventListener != null)
    {
      throw new TooManyListenersException();
    }
    this.SPEventListener = paramSerialPortEventListener;
    if (!this.MonitorThreadAlive)
    {
      this.MonitorThreadLock = true;
      this.monThread = new MonitorThread();
      this.monThread.start();
      waitForTheNativeCodeSilly();
      this.MonitorThreadAlive = true;
    }
  }

  public void removeEventListener()
  {
    waitForTheNativeCodeSilly();

    if (this.monThreadisInterrupted == true)
    {
      z.reportln("\tRXTXPort:removeEventListener() already interrupted");
      this.monThread = null;
      this.SPEventListener = null;
      return;
    }
    if ((this.monThread != null) && (this.monThread.isAlive()))
    {
      this.monThreadisInterrupted = true;

      interruptEventLoop();
      try
      {
        this.monThread.join(1000L);
      }
      catch (Exception localException1) {
        localException1.printStackTrace();
      }

      while (this.monThread.isAlive())
      {
        try
        {
          this.monThread.join(1000L);
          Thread.sleep(1000L);
        }
        catch (Exception localException2)
        {
        }
      }
    }

    this.monThread = null;
    this.SPEventListener = null;
    this.MonitorThreadLock = false;
    this.MonitorThreadAlive = false;
    this.monThreadisInterrupted = true;
    z.reportln("RXTXPort:removeEventListener() returning");
  }

  protected void waitForTheNativeCodeSilly()
  {
    while (this.MonitorThreadLock)
      try
      {
        Thread.sleep(5L);
      }
      catch (Exception localException)
      {
      }
  }

  private native void nativeSetEventFlag(int paramInt1, int paramInt2, boolean paramBoolean);

  public void notifyOnDataAvailable(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();

    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 1, paramBoolean);

    this.monThread.Data = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnOutputEmpty(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 2, paramBoolean);

    this.monThread.Output = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnCTS(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 3, paramBoolean);
    this.monThread.CTS = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnDSR(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 4, paramBoolean);
    this.monThread.DSR = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnRingIndicator(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 5, paramBoolean);
    this.monThread.RI = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnCarrierDetect(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 6, paramBoolean);
    this.monThread.CD = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnOverrunError(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 7, paramBoolean);
    this.monThread.OE = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnParityError(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 8, paramBoolean);
    this.monThread.PE = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnFramingError(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 9, paramBoolean);
    this.monThread.FE = paramBoolean;
    this.MonitorThreadLock = false;
  }

  public void notifyOnBreakInterrupt(boolean paramBoolean)
  {
    waitForTheNativeCodeSilly();
    this.MonitorThreadLock = true;
    nativeSetEventFlag(this.fd, 10, paramBoolean);
    this.monThread.BI = paramBoolean;
    this.MonitorThreadLock = false;
  }

  private native void nativeClose(String paramString);

  public synchronized void close()
  {
    if (this.closeLock) return;
    this.closeLock = true;
    while (this.IOLocked > 0)
    {
      try
      {
        Thread.sleep(500L); } catch (Exception localException) {
      }
    }
    if (this.fd <= 0)
    {
      z.reportln("RXTXPort:close detected bad File Descriptor");
      return;
    }
    setDTR(false);
    setDSR(false);

    if (!this.monThreadisInterrupted)
    {
      removeEventListener();
    }

    nativeClose(this.name);

    super.close();
    this.fd = 0;
    this.closeLock = false;
  }

  protected void finalize()
  {
    if (this.fd > 0)
    {
      close();
    }
    z.finalize();
  }

  /** @deprecated */
  public void setRcvFifoTrigger(int paramInt)
  {
  }

  private static native void nativeStaticSetSerialPortParams(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticSetDSR(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticSetDTR(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticSetRTS(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticIsDSR(String paramString)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticIsDTR(String paramString)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticIsRTS(String paramString)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticIsCTS(String paramString)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticIsCD(String paramString)
    throws UnsupportedCommOperationException;

  private static native boolean nativeStaticIsRI(String paramString)
    throws UnsupportedCommOperationException;

  private static native int nativeStaticGetBaudRate(String paramString)
    throws UnsupportedCommOperationException;

  private static native int nativeStaticGetDataBits(String paramString)
    throws UnsupportedCommOperationException;

  private static native int nativeStaticGetParity(String paramString)
    throws UnsupportedCommOperationException;

  private static native int nativeStaticGetStopBits(String paramString)
    throws UnsupportedCommOperationException;

  private native byte nativeGetParityErrorChar()
    throws UnsupportedCommOperationException;

  private native boolean nativeSetParityErrorChar(byte paramByte)
    throws UnsupportedCommOperationException;

  private native byte nativeGetEndOfInputChar()
    throws UnsupportedCommOperationException;

  private native boolean nativeSetEndOfInputChar(byte paramByte)
    throws UnsupportedCommOperationException;

  private native boolean nativeSetUartType(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException;

  native String nativeGetUartType()
    throws UnsupportedCommOperationException;

  private native boolean nativeSetBaudBase(int paramInt)
    throws UnsupportedCommOperationException;

  private native int nativeGetBaudBase()
    throws UnsupportedCommOperationException;

  private native boolean nativeSetDivisor(int paramInt)
    throws UnsupportedCommOperationException;

  private native int nativeGetDivisor()
    throws UnsupportedCommOperationException;

  private native boolean nativeSetLowLatency()
    throws UnsupportedCommOperationException;

  private native boolean nativeGetLowLatency()
    throws UnsupportedCommOperationException;

  private native boolean nativeSetCallOutHangup(boolean paramBoolean)
    throws UnsupportedCommOperationException;

  private native boolean nativeGetCallOutHangup()
    throws UnsupportedCommOperationException;

  private native boolean nativeClearCommInput()
    throws UnsupportedCommOperationException;

  public static int staticGetBaudRate(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticGetBaudRate(paramString);
  }

  public static int staticGetDataBits(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticGetDataBits(paramString);
  }

  public static int staticGetParity(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticGetParity(paramString);
  }

  public static int staticGetStopBits(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticGetStopBits(paramString);
  }

  public static void staticSetSerialPortParams(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws UnsupportedCommOperationException
  {
    nativeStaticSetSerialPortParams(paramString, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public static boolean staticSetDSR(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException
  {
    return nativeStaticSetDSR(paramString, paramBoolean);
  }

  public static boolean staticSetDTR(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException
  {
    return nativeStaticSetDTR(paramString, paramBoolean);
  }

  public static boolean staticSetRTS(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException
  {
    return nativeStaticSetRTS(paramString, paramBoolean);
  }

  public static boolean staticIsRTS(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticIsRTS(paramString);
  }

  public static boolean staticIsCD(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticIsCD(paramString);
  }

  public static boolean staticIsCTS(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticIsCTS(paramString);
  }

  public static boolean staticIsDSR(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticIsDSR(paramString);
  }

  public static boolean staticIsDTR(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticIsDTR(paramString);
  }

  public static boolean staticIsRI(String paramString)
    throws UnsupportedCommOperationException
  {
    return nativeStaticIsRI(paramString);
  }

  public byte getParityErrorChar()
    throws UnsupportedCommOperationException
  {
    byte b = nativeGetParityErrorChar();

    return b;
  }

  public boolean setParityErrorChar(byte paramByte)
    throws UnsupportedCommOperationException
  {
    return nativeSetParityErrorChar(paramByte);
  }

  public byte getEndOfInputChar()
    throws UnsupportedCommOperationException
  {
    byte b = nativeGetEndOfInputChar();

    return b;
  }

  public boolean setEndOfInputChar(byte paramByte)
    throws UnsupportedCommOperationException
  {
    return nativeSetEndOfInputChar(paramByte);
  }

  public boolean setUARTType(String paramString, boolean paramBoolean)
    throws UnsupportedCommOperationException
  {
    return nativeSetUartType(paramString, paramBoolean);
  }

  public String getUARTType()
    throws UnsupportedCommOperationException
  {
    return nativeGetUartType();
  }

  public boolean setBaudBase(int paramInt)
    throws UnsupportedCommOperationException, IOException
  {
    return nativeSetBaudBase(paramInt);
  }

  public int getBaudBase()
    throws UnsupportedCommOperationException, IOException
  {
    return nativeGetBaudBase();
  }

  public boolean setDivisor(int paramInt)
    throws UnsupportedCommOperationException, IOException
  {
    return nativeSetDivisor(paramInt);
  }

  public int getDivisor()
    throws UnsupportedCommOperationException, IOException
  {
    return nativeGetDivisor();
  }

  public boolean setLowLatency()
    throws UnsupportedCommOperationException
  {
    return nativeSetLowLatency();
  }

  public boolean getLowLatency()
    throws UnsupportedCommOperationException
  {
    return nativeGetLowLatency();
  }

  public boolean setCallOutHangup(boolean paramBoolean)
    throws UnsupportedCommOperationException
  {
    return nativeSetCallOutHangup(paramBoolean);
  }

  public boolean getCallOutHangup()
    throws UnsupportedCommOperationException
  {
    return nativeGetCallOutHangup();
  }

  public boolean clearCommInput()
    throws UnsupportedCommOperationException
  {
    return nativeClearCommInput();
  }

  static
  {
    try
    {
      z = new Zystem();
    }
    catch (Exception localException)
    {
    }
    System.loadLibrary("rxtxSerial");
    Initialize();
  }

  class MonitorThread extends Thread
  {
    private volatile boolean CTS = false;
    private volatile boolean DSR = false;
    private volatile boolean RI = false;
    private volatile boolean CD = false;
    private volatile boolean OE = false;
    private volatile boolean PE = false;
    private volatile boolean FE = false;
    private volatile boolean BI = false;
    private volatile boolean Data = false;
    private volatile boolean Output = false;

    MonitorThread()
    {
    }

    public void run()
    {
      RXTXPort.this.monThreadisInterrupted = false;
      RXTXPort.this.eventLoop();
    }

    protected void finalize()
      throws Throwable
    {
    }
  }

  class SerialInputStream extends InputStream
  {
    SerialInputStream()
    {
    }

    public synchronized int read()
      throws IOException
    {
      if (RXTXPort.this.fd == 0) throw new IOException();
      if (RXTXPort.this.monThreadisInterrupted)
      {
        RXTXPort.z.reportln("+++++++++ read() monThreadisInterrupted");
      }
      RXTXPort.this.IOLocked += 1;

      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        int i = RXTXPort.this.readByte();

        return i;
      }
      finally
      {
        RXTXPort.this.IOLocked -= 1;
      }
    }

    public synchronized int read(byte[] paramArrayOfByte)
      throws IOException
    {
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return 0;
      }
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        int i = read(paramArrayOfByte, 0, paramArrayOfByte.length);

        return i;
      }
      finally
      {
        RXTXPort.this.IOLocked -= 1;
      }
    }

    public synchronized int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      if (RXTXPort.this.fd == 0)
      {
        RXTXPort.z.reportln("+++++++ IOException()\n");
        throw new IOException();
      }

      if (paramArrayOfByte == null)
      {
        RXTXPort.z.reportln("+++++++ NullPointerException()\n");

        throw new NullPointerException();
      }

      if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length))
      {
        RXTXPort.z.reportln("+++++++ IndexOutOfBoundsException()\n");

        throw new IndexOutOfBoundsException();
      }

      if (paramInt2 == 0)
      {
        return 0;
      }

      int j = paramInt2;
      int k;
      if (RXTXPort.this.threshold == 0)
      {
        k = RXTXPort.this.nativeavailable();
        if (k == 0)
          j = 1;
        else {
          j = Math.min(j, k);
        }

      }
      else
      {
        j = Math.min(j, RXTXPort.this.threshold);
      }
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return 0;
      }
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        int i = RXTXPort.this.readArray(paramArrayOfByte, paramInt1, j);

        return i;
      }
      finally
      {
        RXTXPort.this.IOLocked -= 1;
      }
    }

    public synchronized int read(byte[] paramArrayOfByte1, int paramInt1, int paramInt2, byte[] paramArrayOfByte2)
      throws IOException
    {
      if (RXTXPort.this.fd == 0)
      {
        RXTXPort.z.reportln("+++++++ IOException()\n");
        throw new IOException();
      }

      if (paramArrayOfByte1 == null)
      {
        RXTXPort.z.reportln("+++++++ NullPointerException()\n");

        throw new NullPointerException();
      }

      if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte1.length))
      {
        RXTXPort.z.reportln("+++++++ IndexOutOfBoundsException()\n");

        throw new IndexOutOfBoundsException();
      }

      if (paramInt2 == 0)
      {
        return 0;
      }

      int j = paramInt2;
      int k;
      if (RXTXPort.this.threshold == 0)
      {
        k = RXTXPort.this.nativeavailable();
        if (k == 0)
          j = 1;
        else {
          j = Math.min(j, k);
        }

      }
      else
      {
        j = Math.min(j, RXTXPort.this.threshold);
      }
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return 0;
      }
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        int i = RXTXPort.this.readTerminatedArray(paramArrayOfByte1, paramInt1, j, paramArrayOfByte2);

        return i;
      }
      finally
      {
        RXTXPort.this.IOLocked -= 1;
      }
    }

    public synchronized int available()
      throws IOException
    {
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return 0;
      }

      RXTXPort.this.IOLocked += 1;
      try
      {
        int i = RXTXPort.this.nativeavailable();

        return i;
      }
      finally
      {
        RXTXPort.this.IOLocked -= 1;
      }
    }
  }

  class SerialOutputStream extends OutputStream
  {
    SerialOutputStream()
    {
    }

    public void write(int paramInt)
      throws IOException
    {
      if (RXTXPort.this.speed == 0) return;
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return;
      }
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      if (RXTXPort.this.fd == 0)
      {
        RXTXPort.this.IOLocked -= 1;
        throw new IOException();
      }
      try
      {
        RXTXPort.this.writeByte(paramInt, RXTXPort.this.monThreadisInterrupted);
      }
      catch (IOException localIOException)
      {
        RXTXPort.this.IOLocked -= 1;
        throw localIOException;
      }
      RXTXPort.this.IOLocked -= 1;
    }

    public void write(byte[] paramArrayOfByte)
      throws IOException
    {
      if (RXTXPort.this.speed == 0) return;
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return;
      }
      if (RXTXPort.this.fd == 0) throw new IOException();
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        RXTXPort.this.writeArray(paramArrayOfByte, 0, paramArrayOfByte.length, RXTXPort.this.monThreadisInterrupted);
      }
      catch (IOException localIOException)
      {
        RXTXPort.this.IOLocked -= 1;
        throw localIOException;
      }
      RXTXPort.this.IOLocked -= 1;
    }

    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      if (RXTXPort.this.speed == 0) return;
      if (paramInt1 + paramInt2 > paramArrayOfByte.length)
      {
        throw new IndexOutOfBoundsException("Invalid offset/length passed to read");
      }

      byte[] arrayOfByte = new byte[paramInt2];
      System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, 0, paramInt2);

      if (RXTXPort.this.fd == 0) throw new IOException();
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return;
      }
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        RXTXPort.this.writeArray(arrayOfByte, 0, paramInt2, RXTXPort.this.monThreadisInterrupted);
      }
      catch (IOException localIOException)
      {
        RXTXPort.this.IOLocked -= 1;
        throw localIOException;
      }
      RXTXPort.this.IOLocked -= 1;
    }

    public void flush()
      throws IOException
    {
      if (RXTXPort.this.speed == 0) return;
      if (RXTXPort.this.fd == 0) throw new IOException();
      if (RXTXPort.this.monThreadisInterrupted == true)
      {
        return;
      }
      RXTXPort.this.IOLocked += 1;
      RXTXPort.this.waitForTheNativeCodeSilly();
      try
      {
        if (RXTXPort.this.nativeDrain(RXTXPort.this.monThreadisInterrupted)) {
          RXTXPort.this.sendEvent(2, true);
        }

      }
      catch (IOException localIOException)
      {
        RXTXPort.this.IOLocked -= 1;
        throw localIOException;
      }
      RXTXPort.this.IOLocked -= 1;
    }
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.RXTXPort
 * JD-Core Version:    0.6.2
 */