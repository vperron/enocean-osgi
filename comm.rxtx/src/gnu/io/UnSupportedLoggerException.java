package gnu.io;

public class UnSupportedLoggerException extends Exception
{
  public UnSupportedLoggerException()
  {
  }

  public UnSupportedLoggerException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.UnSupportedLoggerException
 * JD-Core Version:    0.6.2
 */