package gnu.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class CommPort
{
  protected String name;
  private static final boolean debug = false;

  public abstract void enableReceiveFraming(int paramInt)
    throws UnsupportedCommOperationException;

  public abstract void disableReceiveFraming();

  public abstract boolean isReceiveFramingEnabled();

  public abstract int getReceiveFramingByte();

  public abstract void disableReceiveTimeout();

  public abstract void enableReceiveTimeout(int paramInt)
    throws UnsupportedCommOperationException;

  public abstract boolean isReceiveTimeoutEnabled();

  public abstract int getReceiveTimeout();

  public abstract void enableReceiveThreshold(int paramInt)
    throws UnsupportedCommOperationException;

  public abstract void disableReceiveThreshold();

  public abstract int getReceiveThreshold();

  public abstract boolean isReceiveThresholdEnabled();

  public abstract void setInputBufferSize(int paramInt);

  public abstract int getInputBufferSize();

  public abstract void setOutputBufferSize(int paramInt);

  public abstract int getOutputBufferSize();

  public void close()
  {
    try
    {
      CommPortIdentifier localCommPortIdentifier = CommPortIdentifier.getPortIdentifier(this);

      if (localCommPortIdentifier != null)
        CommPortIdentifier.getPortIdentifier(this).internalClosePort();
    }
    catch (NoSuchPortException localNoSuchPortException)
    {
    }
  }

  public abstract InputStream getInputStream() throws IOException;

  public abstract OutputStream getOutputStream() throws IOException;

  public String getName()
  {
    return this.name;
  }

  public String toString()
  {
    return this.name;
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.CommPort
 * JD-Core Version:    0.6.2
 */