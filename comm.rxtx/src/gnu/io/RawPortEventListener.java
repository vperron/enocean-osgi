package gnu.io;

import java.util.EventListener;

public abstract interface RawPortEventListener extends EventListener
{
  public abstract void RawEvent(RawPortEvent paramRawPortEvent);
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.RawPortEventListener
 * JD-Core Version:    0.6.2
 */