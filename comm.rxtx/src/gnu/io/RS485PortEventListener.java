package gnu.io;

import java.util.EventListener;

public abstract interface RS485PortEventListener extends EventListener
{
  public abstract void RS485Event(RS485PortEvent paramRS485PortEvent);
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.RS485PortEventListener
 * JD-Core Version:    0.6.2
 */