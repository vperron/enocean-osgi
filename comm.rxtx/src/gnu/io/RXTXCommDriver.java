package gnu.io;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;

public class RXTXCommDriver
  implements CommDriver
{
  private static final boolean debug = false;
  private static final boolean devel = true;
  private String deviceDirectory;
  private String osName;

  private native boolean registerKnownPorts(int paramInt);

  private native boolean isPortPrefixValid(String paramString);

  private native boolean testRead(String paramString, int paramInt);

  private native String getDeviceDirectory();

  private final String[] getValidPortPrefixes(String[] paramArrayOfString)
  {
    String[] arrayOfString1 = new String[256];

    if (paramArrayOfString == null);
    int i = 0;
    for (int j = 0; j < paramArrayOfString.length; j++) {
      if (isPortPrefixValid(paramArrayOfString[j])) {
        arrayOfString1[(i++)] = new String(paramArrayOfString[j]);
      }
    }

    String[] arrayOfString2 = new String[i];
    System.arraycopy(arrayOfString1, 0, arrayOfString2, 0, i);
    if (arrayOfString1[0] == null);
    return arrayOfString2; 
  }
  
  
  private void checkSolaris(String paramString, int paramInt) { // Byte code:
    //   0: iconst_1
    //   1: newarray char
    //   3: dup
    //   4: iconst_0
    //   5: bipush 91
    //   7: castore
    //   8: astore_3
    //   9: aload_3
    //   10: iconst_0
    //   11: bipush 97
    //   13: castore
    //   14: aload_3
    //   15: iconst_0
    //   16: caload
    //   17: bipush 123
    //   19: if_icmpge +51 -> 70
    //   22: aload_0
    //   23: aload_1
    //   24: new 2	java/lang/String
    //   27: dup
    //   28: aload_3
    //   29: invokespecial 6	java/lang/String:<init>	([C)V
    //   32: invokevirtual 7	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   35: iload_2
    //   36: invokespecial 8	gnu/io/RXTXCommDriver:testRead	(Ljava/lang/String;I)Z
    //   39: ifeq +20 -> 59
    //   42: aload_1
    //   43: new 2	java/lang/String
    //   46: dup
    //   47: aload_3
    //   48: invokespecial 6	java/lang/String:<init>	([C)V
    //   51: invokevirtual 7	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   54: iload_2
    //   55: aload_0
    //   56: invokestatic 9	gnu/io/CommPortIdentifier:addPortName	(Ljava/lang/String;ILgnu/io/CommDriver;)V
    //   59: aload_3
    //   60: iconst_0
    //   61: dup2
    //   62: caload
    //   63: iconst_1
    //   64: iadd
    //   65: i2c
    //   66: castore
    //   67: goto -53 -> 14
    //   70: return }
  }
  
  private void registerValidPorts(String[] paramArrayOfString1, String[] paramArrayOfString2, int paramInt) { int i = 0;
    int j = 0;

    if ((paramArrayOfString1 != null) && (paramArrayOfString2 != null))
    {
      for (i = 0; i < paramArrayOfString1.length; i++)
        for (j = 0; j < paramArrayOfString2.length; j++)
        {
          String str1 = paramArrayOfString2[j];
          int k = str1.length();
          String str2 = paramArrayOfString1[i];
          if (str2.length() >= k) {
            String str3 = str2.substring(k).toUpperCase();

            String str4 = str2.substring(k).toLowerCase();

            if ((str2.regionMatches(0, str1, 0, k)) && (str3.equals(str4)))
            {
              String str5;
              if (this.osName.toLowerCase().indexOf("windows") == -1)
              {
                str5 = new String(this.deviceDirectory + str2);
              }
              else
              {
                str5 = new String(str2);
              }

              if ((this.osName.equals("Solaris")) || (this.osName.equals("SunOS")))
              {
                checkSolaris(str5, paramInt);
              } else if (testRead(str5, paramInt))
              {
                CommPortIdentifier.addPortName(str5, paramInt, this);
              }
            }
          }
        }
    }
  }

  public void initialize()
  {
    this.osName = System.getProperty("os.name");
    this.deviceDirectory = getDeviceDirectory();

    for (int i = 1; i <= 2; i++)
      if ((!registerSpecifiedPorts(i)) && 
        (!registerKnownPorts(i)))
        registerScannedPorts(i);
  }

  private void addSpecifiedPorts(String paramString, int paramInt)
  {
    String str1 = System.getProperty("path.separator", ":");
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, str1);

    while (localStringTokenizer.hasMoreElements())
    {
      String str2 = localStringTokenizer.nextToken();

      if (testRead(str2, paramInt))
        CommPortIdentifier.addPortName(str2, paramInt, this);
    }
  }

  private boolean registerSpecifiedPorts(int paramInt)
  {
    String str1 = null;
    Properties localProperties;
    Iterator localIterator;
    try
    {
      String str2 = System.getProperty("java.ext.dirs") + System.getProperty("file.separator");
      FileInputStream localFileInputStream = new FileInputStream(str2 + "gnu.io.rxtx.properties");
      localProperties = new Properties();
      localProperties.load(localFileInputStream);
      System.setProperties(localProperties);
      for (localIterator = localProperties.keySet().iterator(); localIterator.hasNext(); ) {
        String str3 = (String)localIterator.next();
        System.setProperty(str3, localProperties.getProperty(str3));
      }

    }
    catch (Exception localException)
    {
    }

    switch (paramInt) {
    case 1:
      if ((str1 = System.getProperty("gnu.io.rxtx.SerialPorts")) == null)
        str1 = System.getProperty("gnu.io.SerialPorts"); break;
    case 2:
      if ((str1 = System.getProperty("gnu.io.rxtx.ParallelPorts")) == null)
        str1 = System.getProperty("gnu.io.ParallelPorts"); break;
    }

    if (str1 != null) {
      addSpecifiedPorts(str1, paramInt);
      return true;
    }return false;
  }

  private void registerScannedPorts(int paramInt)
  {
    Object localObject1;
    String[] arrayOfString1;
    String[] localObject2;
	if (this.osName.equals("Windows CE"))
    {
      localObject2 = new String[] { "COM1:", "COM2:", "COM3:", "COM4:", "COM5:", "COM6:", "COM7:", "COM8:" };

      localObject1 = localObject2;
    }
    else
    {
      int i;
      if (this.osName.toLowerCase().indexOf("windows") != -1)
      {
        localObject2 = new String[259];
        for (i = 1; i <= 256; i++)
        {
          localObject2[(i - 1)] = new String("COM" + i);
        }
        for (i = 1; i <= 3; i++)
        {
          localObject2[(i + 255)] = new String("LPT" + i);
        }
        localObject1 = localObject2;
      }
      else if ((this.osName.equals("Solaris")) || (this.osName.equals("SunOS")))
      {
        localObject2 = new String[2];
        i = 0;
        File localFile = null;

        localFile = new File("/dev/term");
        if (localFile.list().length > 0);
        localObject2[(i++)] = new String("term/");

        String[] arrayOfString2 = new String[i];
        for (i--; i >= 0; i--)
          arrayOfString2[i] = localObject2[i];
        localObject1 = arrayOfString2;
      }
      else
      {
        arrayOfString1 = ((File)new File(this.deviceDirectory)).list();
        localObject1 = arrayOfString1;
      }
    }
    if (localObject1 == null)
    {
      return;
    }

    localObject2 = new String[0];
    switch (paramInt)
    {
    case 1:
      if (this.osName.equals("Linux"))
      {
        arrayOfString1 = new String[] { "ttyS", "ttySA", "ttyUSB" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("Linux-all-ports"))
      {
        arrayOfString1 = new String[] { "comx", "holter", "modem", "rfcomm", "ttyircomm", "ttycosa0c", "ttycosa1c", "ttyC", "ttyCH", "ttyD", "ttyE", "ttyF", "ttyH", "ttyI", "ttyL", "ttyM", "ttyMX", "ttyP", "ttyR", "ttyS", "ttySI", "ttySR", "ttyT", "ttyUSB", "ttyV", "ttyW", "ttyX" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.toLowerCase().indexOf("qnx") != -1)
      {
        arrayOfString1 = new String[] { "ser" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("Irix"))
      {
        arrayOfString1 = new String[] { "ttyc", "ttyd", "ttyf", "ttym", "ttyq", "tty4d", "tty4f", "midi", "us" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("FreeBSD"))
      {
        arrayOfString1 = new String[] { "ttyd", "cuaa", "ttyA", "cuaA", "ttyD", "cuaD", "ttyE", "cuaE", "ttyF", "cuaF", "ttyR", "cuaR", "stl" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("NetBSD"))
      {
        arrayOfString1 = new String[] { "tty0" };

        localObject2 = arrayOfString1;
      }
      else if ((this.osName.equals("Solaris")) || (this.osName.equals("SunOS")))
      {
        arrayOfString1 = new String[] { "term/", "cua/" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("HP-UX"))
      {
        arrayOfString1 = new String[] { "tty0p", "tty1p" };

        localObject2 = arrayOfString1;
      }
      else if ((this.osName.equals("UnixWare")) || (this.osName.equals("OpenUNIX")))
      {
        arrayOfString1 = new String[] { "tty00s", "tty01s", "tty02s", "tty03s" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("OpenServer"))
      {
        arrayOfString1 = new String[] { "tty1A", "tty2A", "tty3A", "tty4A", "tty5A", "tty6A", "tty7A", "tty8A", "tty9A", "tty10A", "tty11A", "tty12A", "tty13A", "tty14A", "tty15A", "tty16A", "ttyu1A", "ttyu2A", "ttyu3A", "ttyu4A", "ttyu5A", "ttyu6A", "ttyu7A", "ttyu8A", "ttyu9A", "ttyu10A", "ttyu11A", "ttyu12A", "ttyu13A", "ttyu14A", "ttyu15A", "ttyu16A" };

        localObject2 = arrayOfString1;
      }
      else if ((this.osName.equals("Compaq's Digital UNIX")) || (this.osName.equals("OSF1")))
      {
        arrayOfString1 = new String[] { "tty0" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("BeOS"))
      {
        arrayOfString1 = new String[] { "serial" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("Mac OS X"))
      {
        arrayOfString1 = new String[] { "cu.KeyUSA28X191.", "tty.KeyUSA28X191.", "cu.KeyUSA28X181.", "tty.KeyUSA28X181.", "cu.KeyUSA19181.", "tty.KeyUSA19181." };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.toLowerCase().indexOf("windows") != -1)
      {
        arrayOfString1 = new String[] { "COM" };

        localObject2 = arrayOfString1; } break;
    case 2:
      if (this.osName.equals("Linux"))
      {
        arrayOfString1 = new String[] { "lp" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.equals("FreeBSD"))
      {
        arrayOfString1 = new String[] { "lpt" };

        localObject2 = arrayOfString1;
      }
      else if (this.osName.toLowerCase().indexOf("windows") != -1)
      {
        arrayOfString1 = new String[] { "LPT" };

        localObject2 = arrayOfString1;
      }
      else
      {
        arrayOfString1 = new String[0];
        localObject2 = arrayOfString1;
      }
      break;
    }

    registerValidPorts((String[]) localObject1, (String[])localObject2, paramInt);
  }

  public CommPort getCommPort(String paramString, int paramInt)
  {
    try
    {
      switch (paramInt) {
      case 1:
        if (this.osName.toLowerCase().indexOf("windows") == -1)
        {
          return new RXTXPort(paramString);
        }

        return new RXTXPort(this.deviceDirectory + paramString);
      case 2:
        return new LPRPort(paramString);
      }

    }
    catch (PortInUseException localPortInUseException)
    {
    }

    return null;
  }

  public void Report(String paramString)
  {
    System.out.println(paramString);
  }

  static
  {
    System.loadLibrary("rxtxSerial");

    String str1 = RXTXVersion.getVersion();

    String str2 = RXTXVersion.nativeGetVersion();

    System.out.println("Stable Library");
    System.out.println("=========================================");
    System.out.println("Native lib Version = " + str2);
    System.out.println("Java lib Version   = " + str1);

    if (!str1.equals(str2))
    {
      System.out.println("WARNING:  RXTX Version mismatch\n\tJar version = " + str1 + "\n\tnative lib Version = " + str2);
    }
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.RXTXCommDriver
 * JD-Core Version:    0.6.2
 */