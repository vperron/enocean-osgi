package gnu.io;

public class PortInUseException extends Exception
{
  public String currentOwner;

  PortInUseException(String paramString)
  {
    super(paramString);
    this.currentOwner = paramString;
  }

  public PortInUseException()
  {
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.PortInUseException
 * JD-Core Version:    0.6.2
 */