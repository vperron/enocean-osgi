package gnu.io;

public abstract interface CommDriver
{
  public abstract CommPort getCommPort(String paramString, int paramInt);

  public abstract void initialize();
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.CommDriver
 * JD-Core Version:    0.6.2
 */