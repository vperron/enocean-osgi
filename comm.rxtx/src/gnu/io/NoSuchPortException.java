package gnu.io;

public class NoSuchPortException extends Exception
{
  NoSuchPortException(String paramString)
  {
    super(paramString);
  }

  public NoSuchPortException()
  {
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.NoSuchPortException
 * JD-Core Version:    0.6.2
 */