package gnu.io;

import java.util.EventObject;

public class ParallelPortEvent extends EventObject
{
  public static final int PAR_EV_ERROR = 1;
  public static final int PAR_EV_BUFFER = 2;
  private boolean OldValue;
  private boolean NewValue;
  private int eventType;

  public ParallelPortEvent(ParallelPort paramParallelPort, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramParallelPort);
    this.OldValue = paramBoolean1;
    this.NewValue = paramBoolean2;
    this.eventType = paramInt;
  }

  public int getEventType() {
    return this.eventType;
  }

  public boolean getNewValue() {
    return this.NewValue;
  }

  public boolean getOldValue() {
    return this.OldValue;
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.ParallelPortEvent
 * JD-Core Version:    0.6.2
 */