package gnu.io;

import java.io.FileDescriptor;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;

public class CommPortIdentifier
{
  public static final int PORT_SERIAL = 1;
  public static final int PORT_PARALLEL = 2;
  public static final int PORT_I2C = 3;
  public static final int PORT_RS485 = 4;
  public static final int PORT_RAW = 5;
  private String PortName;
  private boolean Available = true;
  private String Owner;
  private CommPort commport;
  private CommDriver RXTXDriver;
  static CommPortIdentifier CommPortIndex;
  CommPortIdentifier next;
  private int PortType;
  private static final boolean debug = false;
  static Object Sync = new Object();
  Vector ownershipListener;
  private boolean HideOwnerEvents;

  CommPortIdentifier(String paramString, CommPort paramCommPort, int paramInt, CommDriver paramCommDriver)
  {
    this.PortName = paramString;
    this.commport = paramCommPort;
    this.PortType = paramInt;
    this.next = null;
    this.RXTXDriver = paramCommDriver;
  }

  public static void addPortName(String paramString, int paramInt, CommDriver paramCommDriver)
  {
    AddIdentifierToList(new CommPortIdentifier(paramString, null, paramInt, paramCommDriver));
  }

  private static void AddIdentifierToList(CommPortIdentifier paramCommPortIdentifier)
  {
    synchronized (Sync)
    {
      if (CommPortIndex == null)
      {
        CommPortIndex = paramCommPortIdentifier;
      }
      else
      {
        CommPortIdentifier localCommPortIdentifier = CommPortIndex;
        while (localCommPortIdentifier.next != null)
        {
          localCommPortIdentifier = localCommPortIdentifier.next;
        }

        localCommPortIdentifier.next = paramCommPortIdentifier;
      }
    }
  }

  public void addPortOwnershipListener(CommPortOwnershipListener paramCommPortOwnershipListener)
  {
    if (this.ownershipListener == null)
    {
      this.ownershipListener = new Vector();
    }

    if (!this.ownershipListener.contains(paramCommPortOwnershipListener))
    {
      this.ownershipListener.addElement(paramCommPortOwnershipListener);
    }
  }

  public String getCurrentOwner()
  {
    return this.Owner;
  }

  public String getName()
  {
    return this.PortName;
  }

  public static CommPortIdentifier getPortIdentifier(String paramString)
    throws NoSuchPortException
  {
    CommPortIdentifier localCommPortIdentifier = CommPortIndex;

    synchronized (Sync)
    {
      while (localCommPortIdentifier != null)
      {
        if (localCommPortIdentifier.PortName.equals(paramString)) break;
        localCommPortIdentifier = localCommPortIdentifier.next;
      }
    }
    if (localCommPortIdentifier != null) return localCommPortIdentifier;

    throw new NoSuchPortException();
  }

  public static CommPortIdentifier getPortIdentifier(CommPort paramCommPort)
    throws NoSuchPortException
  {
    CommPortIdentifier localCommPortIdentifier = CommPortIndex;
    synchronized (Sync)
    {
      while ((localCommPortIdentifier != null) && (localCommPortIdentifier.commport != paramCommPort))
        localCommPortIdentifier = localCommPortIdentifier.next;
    }
    if (localCommPortIdentifier != null) {
      return localCommPortIdentifier;
    }

    throw new NoSuchPortException();
  }

  public static Enumeration getPortIdentifiers()
  {
    CommPortIndex = null;
    try
    {
      CommDriver localCommDriver = (CommDriver)Class.forName("gnu.io.RXTXCommDriver").newInstance();
      localCommDriver.initialize();
    }
    catch (Throwable localThrowable)
    {
      System.err.println(localThrowable + " thrown while loading " + "gnu.io.RXTXCommDriver");
    }
    return new CommPortEnumerator();
  }

  public int getPortType()
  {
    return this.PortType;
  }

  public synchronized boolean isCurrentlyOwned()
  {
    return !this.Available;
  }

  public synchronized CommPort open(FileDescriptor paramFileDescriptor)
    throws UnsupportedCommOperationException
  {
    throw new UnsupportedCommOperationException();
  }

  private native String native_psmisc_report_owner(String paramString);

  public synchronized CommPort open(String paramString, int paramInt)
    throws PortInUseException
  {
    if (!this.Available)
    {
      synchronized (Sync)
      {
        fireOwnershipEvent(3);
        try
        {
          wait(paramInt);
        } catch (InterruptedException localInterruptedException) {
        }
      }
    }
    if (!this.Available)
    {
      throw new PortInUseException(getCurrentOwner());
    }
    if (this.commport == null)
    {
      this.commport = this.RXTXDriver.getCommPort(this.PortName, this.PortType);
    }
    if (this.commport != null)
    {
      this.Owner = paramString;
      this.Available = false;
      fireOwnershipEvent(1);
      return this.commport;
    }

    throw new PortInUseException(native_psmisc_report_owner(this.PortName));
  }

  public void removePortOwnershipListener(CommPortOwnershipListener paramCommPortOwnershipListener)
  {
    if (this.ownershipListener != null)
      this.ownershipListener.removeElement(paramCommPortOwnershipListener);
  }

  synchronized void internalClosePort()
  {
    this.Owner = null;
    this.Available = true;
    this.commport = null;

    notifyAll();
    fireOwnershipEvent(2);
  }

  void fireOwnershipEvent(int paramInt)
  {
    if (this.ownershipListener != null)
    {
      Enumeration localEnumeration = this.ownershipListener.elements();
      CommPortOwnershipListener localCommPortOwnershipListener;
      for (; localEnumeration.hasMoreElements(); 
        localCommPortOwnershipListener.ownershipChange(paramInt))
        localCommPortOwnershipListener = (CommPortOwnershipListener)localEnumeration.nextElement();
    }
  }

  static
  {
    try
    {
      CommDriver localCommDriver = (CommDriver)Class.forName("gnu.io.RXTXCommDriver").newInstance();
      localCommDriver.initialize();
    }
    catch (Throwable localThrowable)
    {
      System.err.println(localThrowable + " thrown while loading " + "gnu.io.RXTXCommDriver");
    }

    String str = System.getProperty("os.name");
    if (str.toLowerCase().indexOf("linux") == -1);
    System.loadLibrary("rxtxSerial");
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.CommPortIdentifier
 * JD-Core Version:    0.6.2
 */