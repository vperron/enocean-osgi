package gnu.io;

public class UnsupportedCommOperationException extends Exception
{
  public UnsupportedCommOperationException()
  {
  }

  public UnsupportedCommOperationException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.UnsupportedCommOperationException
 * JD-Core Version:    0.6.2
 */