package gnu.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.TooManyListenersException;

final class LPRPort extends ParallelPort
{
  private static final boolean debug = false;
  private int fd;
  private final ParallelOutputStream out = new ParallelOutputStream();

  private final ParallelInputStream in = new ParallelInputStream();

  private int lprmode = 0;

  private int timeout = 0;

  private int threshold = 1;
  private ParallelPortEventListener PPEventListener;
  private MonitorThread monThread;

  private static native void Initialize();

  public LPRPort(String paramString)
    throws PortInUseException
  {
    this.fd = open(paramString);
    this.name = paramString;
  }

  private synchronized native int open(String paramString)
    throws PortInUseException;

  public OutputStream getOutputStream()
  {
    return this.out;
  }

  public InputStream getInputStream() {
    return this.in;
  }

  public int getMode()
  {
    return this.lprmode;
  }
  public int setMode(int paramInt) throws UnsupportedCommOperationException {
    try {
      setLPRMode(paramInt);
    } catch (UnsupportedCommOperationException localUnsupportedCommOperationException) {
      localUnsupportedCommOperationException.printStackTrace();
      return -1;
    }
    this.lprmode = paramInt;
    return 0;
  }

  public void restart() {
    System.out.println("restart() is not implemented");
  }

  public void suspend() {
    System.out.println("suspend() is not implemented"); } 
  public native boolean setLPRMode(int paramInt) throws UnsupportedCommOperationException;

  public native boolean isPaperOut();

  public native boolean isPrinterBusy();

  public native boolean isPrinterError();

  public native boolean isPrinterSelected();

  public native boolean isPrinterTimedOut();

  private native void nativeClose();

  public synchronized void close() { if (this.fd < 0) return;
    nativeClose();
    super.close();
    removeEventListener();

    this.fd = 0;
    Runtime.getRuntime().gc();
  }

  public void enableReceiveFraming(int paramInt)
    throws UnsupportedCommOperationException
  {
    throw new UnsupportedCommOperationException("Not supported");
  }
  public void disableReceiveFraming() {  } 
  public boolean isReceiveFramingEnabled() { return false; } 
  public int getReceiveFramingByte() { return 0; }


  public void enableReceiveTimeout(int paramInt)
  {
    if (paramInt > 0) this.timeout = paramInt; else
      this.timeout = 0; 
  }
  public void disableReceiveTimeout() { this.timeout = 0; } 
  public boolean isReceiveTimeoutEnabled() { return this.timeout > 0; } 
  public int getReceiveTimeout() { return this.timeout; }


  public void enableReceiveThreshold(int paramInt)
  {
    if (paramInt > 1) this.threshold = paramInt; else
      this.threshold = 1; 
  }
  public void disableReceiveThreshold() { this.threshold = 1; } 
  public int getReceiveThreshold() { return this.threshold; } 
  public boolean isReceiveThresholdEnabled() { return this.threshold > 1; }


  public native void setInputBufferSize(int paramInt);

  public native int getInputBufferSize();

  public native void setOutputBufferSize(int paramInt);

  public native int getOutputBufferSize();

  public native int getOutputBufferFree();

  protected native void writeByte(int paramInt)
    throws IOException;

  protected native void writeArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException;

  protected native void drain()
    throws IOException;

  protected native int nativeavailable()
    throws IOException;

  protected native int readByte()
    throws IOException;

  protected native int readArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException;

  native void eventLoop();

  public boolean checkMonitorThread()
  {
    if (this.monThread != null)
      return this.monThread.isInterrupted();
    return true;
  }

  public synchronized boolean sendEvent(int paramInt, boolean paramBoolean)
  {
    if ((this.fd == 0) || (this.PPEventListener == null) || (this.monThread == null))
    {
      return true;
    }

    switch (paramInt)
    {
    case 2:
      if (!this.monThread.monBuffer)
        return false; break;
    case 1:
      if (!this.monThread.monError)
        return false; break;
    default:
      System.err.println("unknown event:" + paramInt);
      return false;
    }
    ParallelPortEvent localParallelPortEvent = new ParallelPortEvent(this, paramInt, !paramBoolean, paramBoolean);

    if (this.PPEventListener != null)
      this.PPEventListener.parallelEvent(localParallelPortEvent);
    if ((this.fd == 0) || (this.PPEventListener == null) || (this.monThread == null))
    {
      return true;
    }
    try
    {
      Thread.sleep(50L); } catch (Exception localException) {
    }return false;
  }

  public synchronized void addEventListener(ParallelPortEventListener paramParallelPortEventListener)
    throws TooManyListenersException
  {
    if (this.PPEventListener != null)
      throw new TooManyListenersException();
    this.PPEventListener = paramParallelPortEventListener;
    this.monThread = new MonitorThread();
    this.monThread.start();
  }

  public synchronized void removeEventListener()
  {
    this.PPEventListener = null;
    if (this.monThread != null)
    {
      this.monThread.interrupt();
      this.monThread = null;
    }
  }

  public synchronized void notifyOnError(boolean paramBoolean)
  {
    System.out.println("notifyOnError is not implemented yet");
    this.monThread.monError = paramBoolean;
  }

  public synchronized void notifyOnBuffer(boolean paramBoolean) {
    System.out.println("notifyOnBuffer is not implemented yet");
    this.monThread.monBuffer = paramBoolean;
  }

  protected void finalize()
  {
    if (this.fd > 0) close();
  }

  static
  {
    System.loadLibrary("rxtxParallel");
    Initialize();
  }

  class MonitorThread extends Thread
  {
    private boolean monError = false;
    private boolean monBuffer = false;

    MonitorThread() {
    }
    public void run() { LPRPort.this.eventLoop();
      yield();
    }
  }

  class ParallelInputStream extends InputStream
  {
    ParallelInputStream()
    {
    }

    public int read()
      throws IOException
    {
      if (LPRPort.this.fd == 0) throw new IOException();
      return LPRPort.this.readByte();
    }

    public int read(byte[] paramArrayOfByte) throws IOException {
      if (LPRPort.this.fd == 0) throw new IOException();
      return LPRPort.this.readArray(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2) throws IOException
    {
      if (LPRPort.this.fd == 0) throw new IOException();
      return LPRPort.this.readArray(paramArrayOfByte, paramInt1, paramInt2);
    }

    public int available() throws IOException {
      if (LPRPort.this.fd == 0) throw new IOException();
      return LPRPort.this.nativeavailable();
    }
  }

  class ParallelOutputStream extends OutputStream
  {
    ParallelOutputStream()
    {
    }

    public synchronized void write(int paramInt)
      throws IOException
    {
      if (LPRPort.this.fd == 0) throw new IOException();
      LPRPort.this.writeByte(paramInt);
    }

    public synchronized void write(byte[] paramArrayOfByte) throws IOException {
      if (LPRPort.this.fd == 0) throw new IOException();
      LPRPort.this.writeArray(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public synchronized void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2) throws IOException
    {
      if (LPRPort.this.fd == 0) throw new IOException();
      LPRPort.this.writeArray(paramArrayOfByte, paramInt1, paramInt2);
    }

    public synchronized void flush() throws IOException {
      if (LPRPort.this.fd == 0) throw new IOException();
    }
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.LPRPort
 * JD-Core Version:    0.6.2
 */