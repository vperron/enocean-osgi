package gnu.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.TooManyListenersException;

final class Raw extends RawPort
{
  private int ciAddress;
  static boolean dsrFlag = false;

  private final RawOutputStream out = new RawOutputStream();

  private final RawInputStream in = new RawInputStream();

  private int speed = 9600;

  private int dataBits = 8;

  private int stopBits = 1;

  private int parity = 0;

  private int flowmode = 0;

  private int timeout = 0;

  private int threshold = 0;

  private int InputBuffer = 0;
  private int OutputBuffer = 0;
  private RawPortEventListener SPEventListener;
  private MonitorThread monThread;
  private int dataAvailable = 0;

  private static native void Initialize();

  public Raw(String paramString)
    throws PortInUseException
  {
    this.ciAddress = Integer.parseInt(paramString);
    open(this.ciAddress);
  }

  private native int open(int paramInt)
    throws PortInUseException;

  public OutputStream getOutputStream()
  {
    return this.out;
  }

  public InputStream getInputStream()
  {
    return this.in;
  }

  public void setRawPortParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws UnsupportedCommOperationException
  {
    nativeSetRawPortParams(paramInt1, paramInt2, paramInt3, paramInt4);
    this.speed = paramInt1;
    this.dataBits = paramInt2;
    this.stopBits = paramInt3;
    this.parity = paramInt4;
  }

  private native void nativeSetRawPortParams(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws UnsupportedCommOperationException;

  public int getBaudRate()
  {
    return this.speed;
  }

  public int getDataBits() {
    return this.dataBits;
  }

  public int getStopBits() {
    return this.stopBits;
  }

  public int getParity() {
    return this.parity;
  }

  public void setFlowControlMode(int paramInt)
  {
    try {
      setflowcontrol(paramInt);
    } catch (IOException localIOException) {
      localIOException.printStackTrace();
      return;
    }
    this.flowmode = paramInt;
  }
  public int getFlowControlMode() { return this.flowmode; }


  native void setflowcontrol(int paramInt)
    throws IOException;

  public void enableReceiveFraming(int paramInt)
    throws UnsupportedCommOperationException
  {
    throw new UnsupportedCommOperationException("Not supported");
  }
  public void disableReceiveFraming() {  } 
  public boolean isReceiveFramingEnabled() { return false; } 
  public int getReceiveFramingByte() { return 0; }

  public native int NativegetReceiveTimeout();

  public native boolean NativeisReceiveTimeoutEnabled();

  public native void NativeEnableReceiveTimeoutThreshold(int paramInt1, int paramInt2, int paramInt3);

  public void disableReceiveTimeout()
  {
    enableReceiveTimeout(0);
  }
  public void enableReceiveTimeout(int paramInt) {
    if (paramInt >= 0) {
      this.timeout = paramInt;
      NativeEnableReceiveTimeoutThreshold(paramInt, this.threshold, this.InputBuffer);
    }
    else {
      System.out.println("Invalid timeout");
    }
  }

  public boolean isReceiveTimeoutEnabled() { return NativeisReceiveTimeoutEnabled(); }

  public int getReceiveTimeout() {
    return NativegetReceiveTimeout();
  }

  public void enableReceiveThreshold(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.threshold = paramInt;
      NativeEnableReceiveTimeoutThreshold(this.timeout, this.threshold, this.InputBuffer);
    }
    else
    {
      System.out.println("Invalid Threshold");
    }
  }

  public void disableReceiveThreshold() { enableReceiveThreshold(0); }

  public int getReceiveThreshold() {
    return this.threshold;
  }
  public boolean isReceiveThresholdEnabled() {
    return this.threshold > 0;
  }

  public void setInputBufferSize(int paramInt)
  {
    this.InputBuffer = paramInt;
  }

  public int getInputBufferSize() {
    return this.InputBuffer;
  }

  public void setOutputBufferSize(int paramInt) {
    this.OutputBuffer = paramInt;
  }

  public int getOutputBufferSize() {
    return this.OutputBuffer;
  }

  public native boolean isDTR();

  public native void setDTR(boolean paramBoolean);

  public native void setRTS(boolean paramBoolean);

  private native void setDSR(boolean paramBoolean);

  public native boolean isCTS();

  public native boolean isDSR();

  public native boolean isCD();

  public native boolean isRI();

  public native boolean isRTS();

  public native void sendBreak(int paramInt);

  private native void writeByte(int paramInt)
    throws IOException;

  private native void writeArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2) throws IOException;

  private native void drain() throws IOException;

  private native int nativeavailable() throws IOException;

  private native int readByte() throws IOException;

  private native int readArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2) throws IOException;

  native void eventLoop();

  public void sendEvent(int paramInt, boolean paramBoolean)
  {
    switch (paramInt) {
    case 1:
      this.dataAvailable = 1;
      if (!this.monThread.Data) return;
      break;
    case 2:
      if (!this.monThread.Output)
      {
        return;
      }

      break;
    case 3:
      if (!this.monThread.CTS) return;
      break;
    case 4:
      if (!this.monThread.DSR) return;
      break;
    case 5:
      if (!this.monThread.RI) return;
      break;
    case 6:
      if (!this.monThread.CD) return;
      break;
    case 7:
      if (!this.monThread.OE) return;
      break;
    case 8:
      if (!this.monThread.PE) return;
      break;
    case 9:
      if (!this.monThread.FE) return;
      break;
    case 10:
      if (!this.monThread.BI) return;
      break;
    default:
      System.err.println("unknown event:" + paramInt);
      return;
    }
    RawPortEvent localRawPortEvent = new RawPortEvent(this, paramInt, !paramBoolean, paramBoolean);
    if (this.SPEventListener != null) this.SPEventListener.RawEvent(localRawPortEvent);
  }

  public void addEventListener(RawPortEventListener paramRawPortEventListener)
    throws TooManyListenersException
  {
    if (this.SPEventListener != null) throw new TooManyListenersException();
    this.SPEventListener = paramRawPortEventListener;
    this.monThread = new MonitorThread();
    this.monThread.start();
  }

  public void removeEventListener() {
    this.SPEventListener = null;
    if (this.monThread != null) {
      this.monThread.interrupt();
      this.monThread = null;
    }
  }

  public void notifyOnDataAvailable(boolean paramBoolean) { this.monThread.Data = paramBoolean; } 
  public void notifyOnOutputEmpty(boolean paramBoolean) {
    this.monThread.Output = paramBoolean;
  }
  public void notifyOnCTS(boolean paramBoolean) { this.monThread.CTS = paramBoolean; } 
  public void notifyOnDSR(boolean paramBoolean) { this.monThread.DSR = paramBoolean; } 
  public void notifyOnRingIndicator(boolean paramBoolean) { this.monThread.RI = paramBoolean; } 
  public void notifyOnCarrierDetect(boolean paramBoolean) { this.monThread.CD = paramBoolean; } 
  public void notifyOnOverrunError(boolean paramBoolean) { this.monThread.OE = paramBoolean; } 
  public void notifyOnParityError(boolean paramBoolean) { this.monThread.PE = paramBoolean; } 
  public void notifyOnFramingError(boolean paramBoolean) { this.monThread.FE = paramBoolean; } 
  public void notifyOnBreakInterrupt(boolean paramBoolean) { this.monThread.BI = paramBoolean; }

  private native int nativeClose();

  public void close()
  {
    setDTR(false);
    setDSR(false);
    nativeClose();
    super.close();
    this.ciAddress = 0;
  }

  protected void finalize()
  {
    close();
  }

  public String getVersion()
  {
    String str = "$Id: 988758b9e0cf4ffcaf04061101b3aa85e4c85650 $";
    return str;
  }

  static
  {
    System.loadLibrary("rxtxRaw");
    Initialize();
  }

  class MonitorThread extends Thread
  {
    private boolean CTS = false;
    private boolean DSR = false;
    private boolean RI = false;
    private boolean CD = false;
    private boolean OE = false;
    private boolean PE = false;
    private boolean FE = false;
    private boolean BI = false;
    private boolean Data = false;
    private boolean Output = false;

    MonitorThread() {  } 
    public void run() { Raw.this.eventLoop(); }

  }

  class RawInputStream extends InputStream
  {
    RawInputStream()
    {
    }

    public int read()
      throws IOException
    {
      Raw.this.dataAvailable = 0;
      return Raw.this.readByte();
    }

    public int read(byte[] paramArrayOfByte) throws IOException {
      return read(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2) throws IOException {
      Raw.this.dataAvailable = 0;
      int i = 0; int j = 0;
      int[] arrayOfInt = { paramArrayOfByte.length, Raw.this.InputBuffer, paramInt2 };

      while ((arrayOfInt[i] == 0) && (i < arrayOfInt.length)) i++;
      j = arrayOfInt[i];
      while (i < arrayOfInt.length)
      {
        if (arrayOfInt[i] > 0)
        {
          j = Math.min(j, arrayOfInt[i]);
        }
        i++;
      }
      j = Math.min(j, Raw.this.threshold);
      if (j == 0) j = 1;
      int k = available();
      int m = Raw.this.readArray(paramArrayOfByte, paramInt1, j);
      return m;
    }
    public int available() throws IOException {
      return Raw.this.nativeavailable();
    }
  }

  class RawOutputStream extends OutputStream
  {
    RawOutputStream()
    {
    }

    public void write(int paramInt)
      throws IOException
    {
      Raw.this.writeByte(paramInt);
    }
    public void write(byte[] paramArrayOfByte) throws IOException {
      Raw.this.writeArray(paramArrayOfByte, 0, paramArrayOfByte.length);
    }
    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2) throws IOException {
      Raw.this.writeArray(paramArrayOfByte, paramInt1, paramInt2);
    }
    public void flush() throws IOException {
      Raw.this.drain();
    }
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.Raw
 * JD-Core Version:    0.6.2
 */