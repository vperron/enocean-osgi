package gnu.io;

public class RXTXVersion
{
  private static String Version = "RXTX-2.1-7";

  public static String getVersion()
  {
    return Version;
  }

  public static native String nativeGetVersion();

  static
  {
    System.loadLibrary("rxtxSerial");
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.RXTXVersion
 * JD-Core Version:    0.6.2
 */