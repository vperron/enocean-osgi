package gnu.io;

import java.util.EventListener;

public abstract interface SerialPortEventListener extends EventListener
{
  public abstract void serialEvent(SerialPortEvent paramSerialPortEvent);
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.SerialPortEventListener
 * JD-Core Version:    0.6.2
 */