package gnu.io;

import java.util.EventListener;

public abstract interface I2CPortEventListener extends EventListener
{
  public abstract void I2CEvent(I2CPortEvent paramI2CPortEvent);
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.I2CPortEventListener
 * JD-Core Version:    0.6.2
 */