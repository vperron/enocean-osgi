package gnu.io;

import java.util.EventListener;

public abstract interface ParallelPortEventListener extends EventListener
{
  public abstract void parallelEvent(ParallelPortEvent paramParallelPortEvent);
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.ParallelPortEventListener
 * JD-Core Version:    0.6.2
 */