package gnu.io;

import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.util.logging.Logger;

public class Zystem
{
  public static final int SILENT_MODE = 0;
  public static final int FILE_MODE = 1;
  public static final int NET_MODE = 2;
  public static final int MEX_MODE = 3;
  public static final int PRINT_MODE = 4;
  public static final int J2EE_MSG_MODE = 5;
  public static final int J2SE_LOG_MODE = 6;
  static int mode = 0;
  private static String target;

  public Zystem(int paramInt)
    throws UnSupportedLoggerException
  {
    mode = paramInt;
    startLogger("asdf");
  }

  public Zystem()
    throws UnSupportedLoggerException
  {
    String str = System.getProperty("gnu.io.log.mode");
    if (str != null)
    {
      if ("SILENT_MODE".equals(str))
      {
        mode = 0;
      }
      else if ("FILE_MODE".equals(str))
      {
        mode = 1;
      }
      else if ("NET_MODE".equals(str))
      {
        mode = 2;
      }
      else if ("MEX_MODE".equals(str))
      {
        mode = 3;
      }
      else if ("PRINT_MODE".equals(str))
      {
        mode = 4;
      }
      else if ("J2EE_MSG_MODE".equals(str))
      {
        mode = 5;
      }
      else if ("J2SE_LOG_MODE".equals(str))
      {
        mode = 6;
      }
      else
      {
        try
        {
          mode = Integer.parseInt(str);
        }
        catch (NumberFormatException localNumberFormatException)
        {
          mode = 0;
        }
      }
    }
    else
    {
      mode = 0;
    }
    startLogger("asdf");
  }

  public void startLogger()
    throws UnSupportedLoggerException
  {
    if ((mode == 0) || (mode == 4))
    {
      return;
    }
    throw new UnSupportedLoggerException("Target Not Allowed");
  }

  public void startLogger(String paramString)
    throws UnSupportedLoggerException
  {
    target = paramString;
  }

  public void finalize()
  {
    mode = 0;
    target = null;
  }

  public void filewrite(String paramString)
  {
    try {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(target, "rw");

      localRandomAccessFile.seek(localRandomAccessFile.length());
      localRandomAccessFile.writeBytes(paramString);
      localRandomAccessFile.close();
    } catch (Exception localException) {
      System.out.println("Debug output file write failed");
    }
  }

  public boolean report(String paramString)
  {
    if (mode != 2)
    {
      if (mode == 4)
      {
        System.out.println(paramString);
        return true;
      }
      if (mode != 3)
      {
        if (mode == 0)
        {
          return true;
        }
        if (mode == 1)
        {
          filewrite(paramString);
        } else {
          if (mode == 5)
          {
            return false;
          }
          if (mode == 6)
          {
            Logger.getLogger("gnu.io").fine(paramString);
            return true;
          }
        }
      }
    }
    return false;
  }

  public boolean reportln()
  {
    if (mode != 2)
    {
      if (mode == 4)
      {
        System.out.println();
        return true;
      }
      if (mode != 3)
      {
        if (mode == 0)
        {
          return true;
        }
        if (mode == 1)
        {
          filewrite("\n");
        }
        else if (mode == 5)
        {
          return false;
        }
      }
    }
    return false;
  }

  public boolean reportln(String paramString)
  {
    if (mode != 2)
    {
      if (mode == 4)
      {
        System.out.println(paramString);
        return true;
      }
      if (mode != 3)
      {
        if (mode == 0)
        {
          return true;
        }
        if (mode == 1)
        {
          filewrite(paramString + "\n");
        } else {
          if (mode == 5)
          {
            return false;
          }
          if (mode == 6)
          {
            return true;
          }
        }
      }
    }
    return false;
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.Zystem
 * JD-Core Version:    0.6.2
 */