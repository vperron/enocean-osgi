package gnu.io;

import java.awt.Checkbox;
import java.awt.Frame;
import java.awt.Panel;
import java.io.FileOutputStream;
import java.io.IOException;

class Configure extends Frame
{
  Checkbox[] cb;
  Panel p1;
  static final int PORT_SERIAL = 1;
  static final int PORT_PARALLEL = 2;
  int PortType = 1;

  String EnumMessage = new String("gnu.io.rxtx.properties has not been detected.\n\nThere is no consistant means of detecting ports on this operating System.  It is necessary to indicate which ports are valid on this system before proper port enumeration can happen.  Please check the ports that are valid on this system and select Save");

  private void saveSpecifiedPorts()
  {
    String str2 = new String(System.getProperty("java.home"));
    String str3 = System.getProperty("path.separator", ":");
    String str4 = System.getProperty("file.separator", "/");
    String str5 = System.getProperty("line.separator");
    String str1;
    if (this.PortType == 1) {
      str1 = new String(str2 + str4 + "lib" + str4 + "gnu.io.rxtx.SerialPorts");
    }
    else if (this.PortType == 2) {
      str1 = new String(str2 + "gnu.io.rxtx.ParallelPorts");
    }
    else
    {
      System.out.println("Bad Port Type!");
      return;
    }
    System.out.println(str1);
    try
    {
      FileOutputStream localFileOutputStream = new FileOutputStream(str1);

      for (int i = 0; i < 128; i++)
      {
        if (this.cb[i].getState())
        {
          String str6 = new String(this.cb[i].getLabel() + str3);

          localFileOutputStream.write(str6.getBytes());
        }
      }
      localFileOutputStream.write(str5.getBytes());
      localFileOutputStream.close();
    }
    catch (IOException localIOException)
    {
      System.out.println("IOException!");
    }
  }

  void addCheckBoxes(String paramString)
  {
    for (int i = 0; i < 128; i++)
      if (this.cb[i] != null)
        this.p1.remove(this.cb[i]);
    for (int i = 1; i < 129; i++)
    {
      this.cb[(i - 1)] = new Checkbox(paramString + i);
      this.p1.add("NORTH", this.cb[(i - 1)]);
    }
  }

  public Configure()
  {
  
  }

  public static void main(String[] paramArrayOfString) {
    new Configure();
  }
}

/* Location:           /home/victor/work/orange/smartlab/orange-hab-compat/osgi/bundles/comm.lin-sheeva.jar
 * Qualified Name:     gnu.io.Configure
 * JD-Core Version:    0.6.2
 */