package org.orange.comm.rxtx;

import gnu.io.CommDriver;
import gnu.io.CommPortIdentifier;

import java.util.Enumeration;
import java.util.StringTokenizer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

/**
 * @author Victor PERRON, Mailys ROBIN, Andre BOTTARO, Antonin CHAZALET.
 */
public class Activator implements BundleActivator {

	private static final String driver = "gnu.io.RXTXCommDriver";
	private static final String COMM_DEVICES_LINUX = "/dev/ttyUSB0,/dev/ttyUSB1,/dev/ttyUSB2,/dev/ttyUSB3,/dev/ttyUSB3";
	private static boolean DEBUG = Boolean.getBoolean("mbs.comm.debug");

	private CommDriver commdriver = null;

	private boolean hasPortIdentifier(Enumeration e, String portName) {
		while (e.hasMoreElements()) {
			CommPortIdentifier cpi = (CommPortIdentifier) e.nextElement();
			if ((cpi != null) && (cpi.getName().equals(portName))) {
				return true;
			}
		}
		return false;
	}

	private String getPortIdentifiers() {
		StringBuffer sb = new StringBuffer(30);
		for (Enumeration e = CommPortIdentifier.getPortIdentifiers(); e
				.hasMoreElements();) {
			CommPortIdentifier cpi = (CommPortIdentifier) e.nextElement();
			sb.append(cpi.getName());
			sb.append(' ');
		}
		return sb.toString();
	}

	public void start(BundleContext bundlecontext) throws Exception {
		System.out.println("driver: " + driver);
		System.out.println("COMM_DEVICES_LINUX: " + COMM_DEVICES_LINUX);
		boolean disabled = Boolean.getBoolean("comm.rxtx.disable");
		if (!disabled) {
			try {
				this.commdriver = ((CommDriver) Class.forName(
						"gnu.io.RXTXCommDriver").newInstance());
				this.commdriver.initialize();
			} catch (Throwable t) {
				throw new BundleException(t + " thrown while loading "
						+ "gnu.io.RXTXCommDriver");
			}

			if (DEBUG) {
				System.out.println("[comm-rxtx] start : initial portIDs [ "
						+ getPortIdentifiers() + "]");
			}
			StringTokenizer stringtokenizer = new StringTokenizer(
					System.getProperty("comm.devices",
							"/dev/ttyUSB0,/dev/ttyUSB1,/dev/ttyUSB2,/dev/ttyUSB3,/dev/ttyUSB3"),
					",");

			Enumeration e = CommPortIdentifier.getPortIdentifiers();
			while (stringtokenizer.hasMoreTokens()) {
				String s = stringtokenizer.nextToken().trim();
				if (!hasPortIdentifier(e, s)) {
					CommPortIdentifier.addPortName(s, 1, this.commdriver);
				}
			}

			if (DEBUG) {
				System.out.println("[comm-rxtx] start : custom portIDs [ "
						+ getPortIdentifiers() + "]");
			}
		} else {
			System.out
					.println("[comm-rxtx] start : disabled by framework configuration [comm.rxtx.disable=true]");
		}
	}

	public void stop(BundleContext bundlecontext) {
		if (DEBUG) {
			System.out.println("[comm-rxtx] stop : left portIDs [ "
					+ getPortIdentifiers() + "]");
		}
	}
}
